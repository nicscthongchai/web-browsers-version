// window.alert(`Document Referer:`, document.referrer)

var web = {
  html: document.getElementsByTagName('html')[0],
  body: document.getElementsByTagName('body')[0],
  app: document.getElementById('app'),
  get title () { return document.title },
  set title (name) {
    var contain = '&nbsp;'
    if (name.indexOf(contain) !== -1) {
      var temp = name.split('&nbsp;')
      name = temp[0] + ' ' + temp[1]
    }
    document.title = name
  },
  setStyle: function () {
    web.html.style.height = '100%'
    web.body.style.margin = 0
    web.body.style.height = '100%'
    web.body.style.fontFamily = '\'Courier New\', Courier, monospace'
    web.body.style.background = 'rgba(0, 0, 0, .15)'
    web.app.style.height = '100%'
    web.app.style.display = 'flex'
    web.app.style.justifyContent = 'space-around'
    web.app.style.alignItems = 'center'
    web.app.style.flexDirection = 'column'
    web.app.style.textAlign = 'center'

    web.main = document.getElementById('main')
    web.main.style.maxWidth = '512px'
    web.main.style.background = 'rgba(0, 0, 0, .75)'
    web.main.style.color = 'white'
    web.main.style.borderRadius = '1em'
    web.main.style.padding = '1em'
    web.main.style.margin = '1em'

    web.line = document.getElementById('line')
    web.line.style.border = '.5px solid rgba(255, 255, 255, 0.5)'
    web.line.style.borderRadius = '1px'

    var index
    var element

    var highlightRed = document.getElementsByClassName('highlight-red')
    if (highlightRed && highlightRed.length > 0) {
      for (index = 0; index < highlightRed.length; index++) {
        element = highlightRed[index]
        element.style.color = 'lightpink'
        element.style.fontWeight = 'bold'
        // element.style.textDecoration = 'underline'
      }
    }
    var highlightGreen = document.getElementsByClassName('highlight-green')
    if (highlightGreen && highlightGreen.length > 0) {
      for (index = 0; index < highlightGreen.length; index++) {
        element = highlightGreen[index]
        element.style.color = 'lightGreen'
        element.style.fontWeight = 'bold'
        // element.style.textDecoration = 'underline'
      }
    }
  },
  set icon (href) {
    var link = document.querySelector("link[rel*='icon']") || document.createElement('link')
    link.type = 'image/x-icon'
    link.rel = 'shortcut icon'
    link.href = href
    document.getElementsByTagName('head')[0].appendChild(link)
  }
}

var browser = {
  version: (function () {
    var userAgent = navigator.userAgent
    var temporaryArray = []
    var matchedVersion = userAgent.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []
    if (/trident/i.test(matchedVersion[1])) {
      temporaryArray = /\brv[ :]+(\d+)/g.exec(userAgent) || []
      return 'IE ' + (temporaryArray[1] || '')
    }
    if (matchedVersion[1] === 'Chrome') {
      temporaryArray = userAgent.match(/\b(OPR|Edge|MiuiBrowser)\/(\d+)/)
      if (temporaryArray != null) {
        return temporaryArray
          .slice(1)
          .join(' ')
          .replace('OPR', 'Opera')
      }
    }
    // console.log(matchedVersion)
    // matchedVersion[1] = matchedVersion[1] === 'Chrome' ? 'Google&nbsp;Chrome' : matchedVersion[1]
    // matchedVersion[1] = matchedVersion[1] === 'Firefox' ? 'Mozilla&nbsp;Firefox' : matchedVersion[1]
    matchedVersion = matchedVersion[2] ? [matchedVersion[1], matchedVersion[2]] : [navigator.appName, navigator.appVersion, '-?']
    if ((temporaryArray = userAgent.match(/version\/(\d+)/i)) != null) matchedVersion.splice(1, 1, temporaryArray[1])
    return matchedVersion.join(' ')
  })()
}

web.title = 'Web Browser Version'

var message = ''
message += '<div id="main">'
message += 'Your\'re using '
message += '<span class="highlight-green">' + browser.version + '</span><hr id="line" />'
message += '<span style="font-size: .75rem;">UserAgent is: '
message += '<span class="highlight-red">' + navigator.userAgent + '</span></span>'
message += '</div>'
web.app.innerHTML = message

web.setStyle()

if (browser.version.split(' ')[0] === 'Chrome') {
  web.icon = 'https://www.google.com/images/icons/product/chrome-32.png'
} else if (browser.version.split(' ')[0] === 'Firefox') {
  web.icon = 'https://www.mozilla.org/media/img/firefox/favicon-196.c6d9abffb769.png'
} else if (browser.version.split(' ')[0] === 'Edge') {
  web.icon = 'https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/REZlo1?ver=5722'
} else if (browser.version.split(' ')[0] === 'IE') {
  web.icon = 'https://c.s-microsoft.com/th-th/CMSImages/ie_symbol_clr_56x56.png?version=73aa5bf1-0743-11e7-8f71-718675c983bf'
}
